<?php

  $cookies_com = array(
    "imi-domain-api" => "api.cueconnect.com",
    "imi-domain-cdns-nonsecure" => "d3a2pyvvxxzj9z.cloudfront.net,d3a2pyvvxxzj9z.cloudfront.net",
    "imi-domain-cdns-secure" => "www.cueconnect.com",
    "imi-domain-consumer" => "www.cueconnect.com",
    "imi-domain-merchant" => "business.cueconnect.com",
    "imi-domain-oauth" => "oauth.cueconnect.com",
    "imi-domain-onebutton" => "onebutton.cueconnect.com",
    "imi-domain-poweredby" => "stream.cueconnect.com",
    "imi-domain-shop" => "shop.cueconnect.com",
    "imi-loc" => "prod",
    "imi-location" => "prod",
  );       

  $cookies_net = array(
    "imi-domain-api" => "_prefix_-api.cueconnect.com",
    "imi-domain-cdns-nonsecure" => "_prefix_-stream.cueconnect.net",
    "imi-domain-cdns-secure" => "www.cueconnect.net",
    "imi-domain-consumer" => "_prefix_-www.cueconnect.net",
    "imi-domain-merchant" => "_prefix_-business.cueconnect.net",
    "imi-domain-oauth" => "_prefix_-oauth.cueconnect.net",
    "imi-domain-onebutton" => "_prefix_-onebutton.cueconnect.net",
    "imi-domain-poweredby" => "_prefix_-stream.cueconnect.net",
    "imi-domain-shop" => "_prefix_-shop.cueconnect.net",
    "imi-loc" => "_prefix_",
    "imi-location" => "_prefix_"
  );

  if (isset($_POST['env'])) {
    switch ($_POST['env']) {
      case 'qa' :
        set_env($cookies_net, 'qa');
      break;
      case 'prod' :
        set_env($cookies_com);
      break;
      case 'other' :
        if (!empty(trim($_POST['dev']))) {
          set_env($cookies_net, $_POST['dev']);
        }
      break;
      case 'cancel' :
        foreach ($cookies_net as $name => $value) {
          setcookie($name, null, -1, '/', '127.0.0.1');
        }
      break;
    }
  }

  function set_env($array, $env = null) {
    foreach($array as $name => $value) {
      if ($env) {
        $value = str_replace('_prefix_', $env, $value);
      }
      setcookie($name, $value, time()+3600*24*30, "/", "127.0.0.1");
    }
    header("Location: http://127.0.0.1:8080/envselect.php");    
  }

?>

<!DOCTYPE html>
<html>
<head>
<title>Cueconnect Envselect</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script srс="http://env.cueconnect.net/js/envselect.js"></script>
</head>

<body>

  <form method="post" class="imi-envselect">
    <input type="submit" name="env" value="prod" />
    <input type="submit" name="env" value="qa" />
    <input type="text" name="dev" value=""/>
    <input type="submit" name="env" value="other" />
    <input type="submit" name="env" value="cancel" />
  </form>

  <hr>

  <h2>current settings:</h2>

  <pre>
    <?php print_r($_COOKIE); ?>
  </pre>

  <br>
  <p>Put <b>"true"</b> for Temporary account or <b>"false"</b> for a mandatory singin process</p>

  <form action="" method="POST">
    <input type="text" name="ob_path">
    <input type="submit" value="Change">
  </form>

<?php

$expire = time() + 60 * 60 * 24 * 30;

if (isset($_POST['ob_path'])){
  setcookie("ob_path", "", time()-3600, '/', '127.0.0.1:8080');
  if ($_POST['ob_path'] == "true") {
    setcookie("ob_path", "true", $expire, '/', '127.0.0.1:8080');
    echo "Ob Mode : With temporary account";
  } else {
    setcookie("ob_path", "false", $expire, '/', '127.0.0.1:8080'); 
    echo "Ob Mode : Without a temporary account";
  }
}

?>

</body>
</html>

